﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace WpfApplicationHttpRequest
{
    class MainWindowViewModel : INotifyPropertyChanged
    {        
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods Public
        public MainWindowViewModel()
        {
            _page = new Page()
            {
                Url = "http://www.contman.pl/"
            };
            
        }
        #endregion 

        #region Property
        public string Url
        {
            get { return _page.Url; }
            set
            {
                _page.Url = value;
                OnPropertyChanged("Url");
            }
        }

        public string Data
        {
            get { return _page.Data; }
            set
            {
                _page.Data = value;
                OnPropertyChanged("Data");
            }
        }

        public ICommand ShowStatus
        {
            get
            {
                if (null == _showStatus)
                {
                    _showStatus = new RelayCommand(ShowStatusExecute, CanShowStatusExecute);
                }
                return _showStatus;
            }
        }        
        #endregion      
        #region Field
        private readonly Page _page;
        private ICommand _showStatus;
        #endregion
        #region Methods Private
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ShowStatusExecute()
        {
            WebClientStatistics webClientStatistics = new WebClientStatistics(_page.Url);
            Data = "Proszę czekać trwa nawiązywanie połączenia.";
            var bw = new BackgroundWorker();
             
            bw.DoWork += (sender, args) => {             
                if(!webClientStatistics.Load())
                {
                    Data = "Błąd połaczenia.";
                }
            };

            bw.RunWorkerCompleted += (sender, args) =>
            {
                if (args.Error != null)  MessageBox.Show(args.Error.ToString());
                if (webClientStatistics.IsLoadedPage)
                {
                    Dictionary<string, int> statistics = webClientStatistics.GetStatisticKeywords();
                    if (statistics.Count > 0)
                    {                        
                        StringBuilder _data = new StringBuilder();
                        foreach (KeyValuePair<string, int> dataItem in statistics)
                        {
                            _data.Append(dataItem.Key);
                            _data.Append(':');
                            _data.AppendLine(dataItem.Value.ToString());                            
                        }
                        Data = _data.ToString();
                    }
                    else
                    {
                        Data = "Brak słów kluczowych.";
                    }
                }                                        
            };

            bw.RunWorkerAsync(); 
                      
        }

        private bool CanShowStatusExecute()
        {
            return !string.IsNullOrEmpty(_page.Url);
        }
        #endregion     
    }
}
