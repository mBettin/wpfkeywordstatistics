﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WpfApplicationHttpRequest
{
    class KeywordsStatistic
    {
        #region Field
        private readonly string htmlCode;
        private string[] keywords;
        #endregion
        #region Methods Public
        public KeywordsStatistic(string htmlCode)
        {            
            this.htmlCode = htmlCode;
            keywords = new string[0];
            GetKeywords();
        }

        public Dictionary<string, int> GetStatistic()
        {
            Dictionary<string, int> statistics = new Dictionary<string, int>();
            string bodyHtmlCode = htmlCode.Substring(htmlCode.IndexOf("<body"));
            for (int i = 0 ; i < keywords.Length; i++)
            {
                MatchCollection mc = Regex.Matches(bodyHtmlCode, "\\s" + keywords[i] + "[\\s\\.,\\?]");
                if (!statistics.ContainsKey(keywords[i]))
                {
                    statistics.Add(keywords[i], mc.Count);                
                }                                
            }
                        
            return statistics;
        }
        #endregion        
        #region Methods Private
        private void GetKeywords()
        {
            var content = "<meta name=\"keywords\" content=\"";
            int startIndex = htmlCode.ToString().IndexOf(content) + content.Length;           
            int endIndex = htmlCode.ToString().IndexOf("\"", startIndex);
            if (startIndex > content.Length)
            {
                keywords = Array.ConvertAll(htmlCode.ToString().Substring(startIndex, endIndex - startIndex).Split(','), p => p.Trim());
            }
        }
        #endregion
    }
}
