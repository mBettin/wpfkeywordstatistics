﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;


namespace WpfApplicationHttpRequest
{
    class WebClientStatistics
    {
        #region Field
        private string url;
        private bool _isLoadedPage;
        private string htmlCode;

        private KeywordsStatistic keywordStatistic;
        #endregion

        #region Propertys
        public bool IsLoadedPage
        {
            get
            {
                return _isLoadedPage;
            }
        }
        #endregion

        #region Methods Public
        public WebClientStatistics(string url)
        {
            this.url = url;
        }

        public bool Load()
        {
            _isLoadedPage = true;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream receiveStream = response.GetResponseStream();
                    StreamReader readStream = null;

                    if (response.CharacterSet == null)
                    {
                        readStream = new StreamReader(receiveStream);
                    }
                    else
                    {
                        readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                    }

                    htmlCode = readStream.ReadToEnd();

                    response.Close();
                    readStream.Close();
                    htmlCode = htmlCode.ToLower();
                }
                else
                {
                    _isLoadedPage = false;
                }
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString());
                _isLoadedPage = false;
            }

           
            return _isLoadedPage;
        }

        public Dictionary<string, int> GetStatisticKeywords()
        {
                keywordStatistic = new KeywordsStatistic(htmlCode);
                return keywordStatistic.GetStatistic();                      
        }
        #endregion
    }
}
